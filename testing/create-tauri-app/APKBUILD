# Contributor: Matthias Ahouansou <matthias@ahouansou.cz>
# Maintainer: Matthias Ahouansou <matthias@ahouansou.cz>
pkgname=create-tauri-app
pkgver=4.3.0
pkgrel=0
pkgdesc="Build tool for Leptos"
url="https://tauri.app"
# loongarch64: blocked by libc crate
arch="all !s390x !loongarch64" # nix
license="MIT OR Apache-2.0"
makedepends="cargo-auditable"
depends="cargo"
subpackages="$pkgname-doc"
source="
	$pkgname-$pkgver.tar.gz::https://github.com/tauri-apps/create-tauri-app/archive/refs/tags/create-tauri-app-v$pkgver.tar.gz
"
options="net"
builddir="$srcdir/$pkgname-$pkgname-v$pkgver"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm 755 target/release/cargo-create-tauri-app "$pkgdir"/usr/bin/cargo-create-tauri-app

	for l in _APACHE-2.0 _MIT .spdx
	do
		install -Dm 644 LICENSE"$l" "$pkgdir"/usr/share/licenses/"$pkgname"/LICENSE"$l"
	done
}

sha512sums="
0d8ee2cc90e93b4f3affee6bfe6695072a0543150a933f89f35d53d0f7739595f2b01975f7a662d12d52352c73065563c1d6638e51bef662a21c1d76715f6552  create-tauri-app-4.3.0.tar.gz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=merkuro
pkgver=24.08.0
pkgrel=1
pkgdesc="A calendar application using Akonadi to sync with external services (NextCloud, GMail, ...)"
# armhf blocked by qt6-qtdeclarative
# ppc64le, s390x, riscv64 and armv7 blocked by qt6-qtwebengine -> akonadi
# loongarch64 blocked by kdepim-runtime
arch="all !armhf !ppc64le !s390x !riscv64 !armv7 !loongarch64"
url="https://invent.kde.org/pim/kalendar"
license="GPL-3.0-or-later AND BSD-2-Clause"
depends="
	kdepim-runtime
	kirigami-addons
	kirigami
	qt6-qtlocation
	"
makedepends="
	akonadi-contacts-dev
	akonadi-dev
	eventviews-dev
	extra-cmake-modules
	kcalendarcore-dev
	kconfigwidgets-dev
	kcontacts-dev
	kcoreaddons-dev
	ki18n-dev
	kidentitymanagement-dev
	kirigami-addons-dev
	kirigami-dev
	kitemmodels-dev
	kpackage-dev
	kpeople-dev
	kwindowsystem-dev
	mailcommon-dev
	mimetreeparser-dev
	qqc2-desktop-style-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	qt6-qtlocation-dev
	qt6-qtsvg-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/pim/merkuro.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/merkuro-$pkgver.tar.xz
	0001-merkuro-Add-missing-GENERATE_PLUGIN_SOURCE-to-merkuro_contact_plugin.patch
	"
# No tests
options="!check"

provides="kalendar=$pkgver-r$pkgrel"
replaces="kalendar"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
79da364ce5d1cba4cbe85d5e91d235af05a18951f9dac5c97002702d0817b9bde3766c426e3c6720b95d6590f6419476d06aff50526656c219376de1f2372348  merkuro-24.08.0.tar.xz
b0ed5d032df33f62dfe740eea12ae4471a4e256c5043ed19422fa4b9ca2ef40d304ab53706657c04a565d3fc41358ed4ea40ea5e058974a497d4724e772c69cd  0001-merkuro-Add-missing-GENERATE_PLUGIN_SOURCE-to-merkuro_contact_plugin.patch
"

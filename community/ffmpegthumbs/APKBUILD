# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=ffmpegthumbs
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# not useful on s390x
arch="all !s390x !armhf"
url="https://www.kde.org/applications/multimedia/"
pkgdesc="FFmpeg-based thumbnail creator for video files"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	ffmpeg-dev
	kconfig-dev
	ki18n-dev
	kio-dev
	qt6-qtbase-dev
	samurai
	taglib-dev
	"
_repo_url="https://invent.kde.org/multimedia/ffmpegthumbs.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/ffmpegthumbs-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DBUILD_WITH_QT6=ON \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d0a9844cabcd18bf1cbf36e82fd7d71ea0804488d7741262a885775b6d64d9a852d1bc5779858702244ad4aa95de36bbd7e0682e62cf2335dd0ba62d084678d7  ffmpegthumbs-24.08.0.tar.xz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=ksirk
pkgver=24.08.0
pkgrel=0
pkgdesc="A computerised version of the well known strategic board game Risk"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://apps.kde.org/en-gb/ksirk/"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	kwallet-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdegames-dev
	qca-dev
	qt6-qtbase-dev
	qt6-qtmultimedia-dev
	qt6-qtsvg-dev
	samurai
	"
_repo_url="https://invent.kde.org/games/ksirk.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/ksirk-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DWITH_JABBER_SUPPORT=OFF # xmpp support isn't Qt6 compatible
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
b21fc63f8502093d7d006cdd53b01b4986d8b9ef57e5ab2184caa122416c1fd637e76176bac905b3715ef752caaf5a3d3fa7c2121bcf05afce6a862e7c4bc984  ksirk-24.08.0.tar.xz
"

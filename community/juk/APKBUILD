# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=juk
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://juk.kde.org/"
pkgdesc="A jukebox, tagger and music collection manager"
license="GPL-2.0-or-later"
depends="phonon-backend-gstreamer"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kjobwidgets-dev
	knotifications-dev
	kstatusnotifieritem-dev
	ktextwidgets-dev
	kwallet-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	phonon-dev
	qt6-qtbase-dev
	qt6-qtsvg-dev
	samurai
	taglib-dev
	"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/multimedia/juk.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/juk-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
cb9f1b4bc4dcc8e4a7b2bcf8af64d6f2cb9ccf0f8e160e57943b8db46d45059a48bc13787031c40a27ff4f6a1bd31ab7268061069f9721ae34bdf5a72187ba5a  juk-24.08.0.tar.xz
"

# Contributor: Carter Li <zhangsongcui@live.cn>
# Maintainer: Carter Li <zhangsongcui@live.cn>
pkgname=fastfetch
pkgver=2.22.0
pkgrel=0
pkgdesc="Like neofetch, but much faster because written mostly in C."
url="https://github.com/fastfetch-cli/fastfetch"
arch="all"
license="MIT"
depends="
	hwdata-pci
	"
makedepends="
	cmake samurai
	yyjson-dev
	yyjson-static
	vulkan-loader-dev
	libxcb-dev
	wayland-dev
	libdrm-dev
	dconf-dev
	imagemagick-dev
	chafa-dev
	zlib-dev
	dbus-dev
	mesa-dev
	opencl-dev
	xfconf-dev
	sqlite-dev
	pulseaudio-dev
	ddcutil-dev
	elfutils-dev
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/fastfetch-cli/fastfetch/archive/refs/tags/$pkgver.tar.gz"


prepare() {
	default_prepare

	rm -rf src/3rdparty/yyjson
}

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		local crossopts="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=Release \
		-DENABLE_SYSTEM_YYJSON=ON \
		-DENABLE_DIRECTX_HEADERS=OFF \
		-DCMAKE_C_FLAGS="$CFLAGS -Wno-stringop-overflow -Wno-maybe-uninitialized" \
		$crossopts
	cmake --build build --target fastfetch --target flashfetch
}

check() {
	build/fastfetch --list-features
	build/fastfetch -c presets/ci.jsonc
	build/fastfetch -c presets/ci.jsonc --format json
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
edb4532d220e031d666eac3ae12a004e1481a7c36c68a94b17323953822d0cedb80cdf34c84a184766e9bad64089aaadc3828b5e13f8867e961533c6919dbeae  fastfetch-2.22.0.tar.gz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kleopatra
pkgver=24.08.0
pkgrel=0
arch="all !armhf !s390x"
url="https://www.kde.org/applications/utilities/kleopatra/"
pkgdesc="Certificate Manager and Unified Crypto GUI"
license="GPL-2.0-or-later AND GFDL-1.2-only"
depends="
	gnupg
	pinentry-qt
	"
makedepends="
	boost-dev
	extra-cmake-modules
	gpgme-dev
	kcmutils-dev
	kcodecs-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kitemmodels-dev
	kmbox-dev
	kmime-dev
	kstatusnotifieritem-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libassuan-dev
	libkleo-dev
	mimetreeparser-dev
	qgpgme
	qt6-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/pim/kleopatra.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kleopatra-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# kuniqueservicetest requires running dbus
	xvfb-run ctest --test-dir build --output-on-failure -E "kuniqueservicetest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3c938ea3b294aae8b608ae916d727db99665a0a637970cb25d2ddd7e17b00d4976881cae1513e8aeca4d71a3ad063449925e3e9ef3bec2d399945328ab71b788  kleopatra-24.08.0.tar.xz
"

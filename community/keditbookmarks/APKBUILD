# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=keditbookmarks
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/"
pkgdesc="Bookmark Organizer and Editor"
license="GPL-2.0-only AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kbookmarks-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kparts-dev
	kwindowsystem-dev
	qt6-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/utilities/keditbookmarks.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/keditbookmarks-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2dee534998e97f986ed09be69a6fba8c4e66f98e60466a5c808be72774b3968662378f04f82a3ec307662eadad13fe6d4a3861c49fc3d80e19b17b4b82a42a1d  keditbookmarks-24.08.0.tar.xz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=libkdepim
pkgver=24.08.0
pkgrel=0
pkgdesc="Lib for common KDEPim apps"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x, riscv64 and loongarch64 blocked by qt6-qtwebengine -> akonadi
arch="all !armhf !ppc64le !s390x !riscv64 !loongarch64"
url="https://community.kde.org/KDE_PIM"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
depends_dev="
	akonadi-contacts-dev
	akonadi-dev
	akonadi-search-dev
	kcmutils-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemviews-dev
	kjobwidgets-dev
	kldap-dev
	kwallet-dev
	kwidgetsaddons-dev
	qt6-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	qt6-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-dev $pkgname-lang"
_repo_url="https://invent.kde.org/pim/libkdepim.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkdepim-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_DESIGNERPLUGIN=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
0314d8f3a96761bc3752bb06f10af15ca5db48871f5e6ea952eb29000c8ecadec19bcf5fdaa62b85bc1f2b3f1c7ffab87f68e618d35ce84e673c54ee411d1016  libkdepim-24.08.0.tar.xz
"
